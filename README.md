# WordPress-Gulp

**[NO LONGER MAINTAINED] Check [WordPress boilerplate](https://github.com/ArmandPhilippot/wordpress-boilerplate) instead.**

Gulp files to build WordPress projects (theme or plugin).

## Introduction

This Gulp configuration is inspired by [WPGulp from Ahmad Awais](https://github.com/ahmadawais/WPGulp). It will implement:

1. Live reloads browser with BrowserSync.
2. CSS: Sass to CSS conversion, error catching, Autoprefixing, Sourcemaps, CSS minification.
3. JS: Concatenates & uglifies JS files.
4. Images: Compress PNG, JPEG, GIF and SVG images.
5. Translate: Generates .pot file for i18n and l10n.
6. Watches files for changes in SCSS, CSS, JS, PHP & Images.
7. Create a zip file containing all files needed for production only.

## Installation

Download all the files in this repository and place them in the root directory of your project (theme, plugin...).

## Usage

Make sure [NodeJS and NPM](https://nodejs.org/en/download/package-manager/) are installed on your system. Then:

```
npm install
```

Running this command will install all packages needed to run Gulp tasks.

Open the gulp.config.js file and configure the project paths and other variables. Replace the variables as per your project requirements.

Three scripts are defined in package.json file:

- watch
- build
- zipfolder

**Examples:**

```
npm watch .
```

This command will monitor changes in your files and perform the corresponding tasks to keep your project up to date.

```
npm build
```

This command will delete the "assets" folder and launch all the tasks necessary for building your project.

```
npm zipfolder
```

This task will create a zip folder containing all the files necessary for production. Package files, gulp, etc. will not be included.

## Disclaimer

I use this as base for my projects (themes and plugins). These tasks may not meet your needs or your workflow.

## Changelog

### 1.0

- Stable version

## License

The WordPress-Gulp repository is licensed under the GPL v2 or later. A copy of the license is included in the root of the plugin’s directory. The file is named LICENSE.
